import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[];


  // injection the service as the constructor's argument
  constructor() { }

  ngOnInit(): void {

    // initialize your getEmployees() method

  }


  // assign your property to the getEmployees() method from your service
  getEmployees() {
  }


  onSelect(employee: Employee) {
  }

}
