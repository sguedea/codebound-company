import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor() { }

  // returns all of the Employees from the data directory inside of the assets directory
  getEmployees(): Employee[] {
    return EMPLOYEES;
  }
}
