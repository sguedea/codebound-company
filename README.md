# INSTRUCTIONS
CodeBound is updating their employee database application!
You have been assigned to create a proposal for the updated company's database webpage.

# INSTRUCTIONS
1. Fork this repository
2. Clone the forked repository.
3. Read through the existing code that is already on here.
4. Make sure that you're in an Angular environment
5. You may need to install Material, and Bootstrap again (this is a new project)


# TODO 
1. Follow and complete the provided instructions in the code
2. Retrieve the proper data
3. Update the company's database template (make the display look better!)
4. Include any additional feature you seem that will make the application better.
